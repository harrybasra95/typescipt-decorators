import 'reflect-metadata';

const mappingSymbol = Symbol('mapping');

function logger(
     _target: any,
     _propertyKey: string,
     descriptor: PropertyDescriptor
) {
     const original = descriptor.value;
     const designParamTypes = Reflect.getMetadata(
          'design:paramtypes',
          _target,
          _propertyKey
     );
     descriptor.value = function (args: any) {
          Object.keys(args).forEach((key, index) => {
               const arg = args[key];
               const paramType = designParamTypes[0];

               const mapping = Reflect.getMetadata(
                    mappingSymbol,
                    paramType.prototype,
                    arg
               );
               // console.log(mapping);
               // const result =
               //      arg.constructor === paramType || arg instanceof paramType;

               if (!mapping) {
                    throw 'No mapping';
               }
          });
          const result = original.call(this, ...args);
          return result;
     };
}

function Map(mapping: boolean) {
     return function (target: Object, propertyKey: string) {
          Reflect.defineMetadata(mappingSymbol, mapping, target, propertyKey);
     };
}
class typeX {
     @Map(true)
     x: number;
     y: number;
}

class Base {
     public toModel() {}
}

class C extends Base {
     @logger
     add({ x, y }: typeX) {
          return x + y;
     }
}

const c = new C();
c.add({ x: 1, y: 2 });
