import 'reflect-metadata';
type KeyPropTypes = {
     mapping?: boolean;
};

function create(props: KeyPropTypes) {
     return function (target: Object, propertyKey: string) {
          let value: string;
          const getter = function (this: { [name: string]: any }) {
               return value;
          };
          const setter = function (newVal: string) {
               console.log(newVal);
               Object.defineProperty(target, `${propertyKey}_props`, {
                    value: {
                         fieldName: 'propertyKey',
                         value: newVal,
                         ...props,
                    },
               });
          };
          Object.defineProperty(target, propertyKey, {
               get: getter,
               set: setter,
               enumerable: true,
               configurable: true,
          });
     };
}

class User {
     constructor() {
          console.log('this works');
     }
     @create({ mapping: true })
     name: string;

     @create({ mapping: true })
     email: string;
}

class Router {
     constructor() {}
     public async createUser(data: User) {
          console.log(data);
     }
}

const router = new Router();
router.createUser({ name: 'harry', email: 'asds' });
