import 'reflect-metadata';
type KeyPropTypes = {
     mapping?: boolean;
};

function create(props: KeyPropTypes) {
     return function (target: Object, propertyKey: string) {
          let value: string;
          const getter = function (this: { [name: string]: any }) {
               return value;
          };
          const setter = function (newVal: string) {
               Object.defineProperty(target, `${propertyKey}_props`, {
                    value: {
                         fieldName: propertyKey,
                         value: newVal,
                         ...props,
                    },
               });
          };
          Object.defineProperty(target, propertyKey, {
               get: getter,
               set: setter,
               enumerable: true,
               configurable: true,
          });
     };
}

function Router<T extends { new (...args: any[]): {} }>(Base: T) {
     return class extends Base {
          constructor(...args: any[]) {
               super(...args);
               const subMethods = Base.prototype;
               Object.keys(subMethods).forEach((key) => {
                    var method = subMethods[key];
                    Object.defineProperty(Base.prototype, key, {
                         value: function () {
                              const user = new User();
                              const args = arguments[0];
                              Object.keys(args).forEach(function (key) {
                                   user[key] = args[key];
                                   console.log(user[`${key}_props`]);
                              });
                              return method.apply(this, arguments);
                         },
                    });
               });
          }
     };
}

class User {
     constructor() {}
     @create({ mapping: true })
     name: string;

     @create({ mapping: true })
     email: string;
}

@Router
class UserRouter {
     constructor() {}
     public async createUser(data: User) {
          console.log(data);
     }
}

const router = new UserRouter();
router.createUser({ name: 'harry', email: 'asds' });
